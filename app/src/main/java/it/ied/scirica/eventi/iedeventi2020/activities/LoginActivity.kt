package it.ied.scirica.eventi.iedeventi2020.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.helpers.AlertHelper
import it.ied.scirica.eventi.iedeventi2020.helpers.AlertHelper.Companion.showToast
import it.ied.scirica.eventi.iedeventi2020.helpers.LoginHelper
import it.ied.scirica.eventi.iedeventi2020.helpers.NetworkParser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        title = "Login"

        button_login.setOnClickListener {
            performLogin()
        }

        //Codice per velocizzare lo sviluppo
        text_email.setText("antonio.scirica@ied.edu")
        text_password.setText("passwordApp")
    }

    fun performLogin()
    {
        //Indirizzo del servizio web da chiamare
        val url = "http://ied.apptoyou.it/app/login.php"
        val parameters = HashMap<String, String>()
        parameters["email"] = text_email.text.toString()
        parameters["password"] = text_password.text.toString()



        //Creo la richiesta
        val request = object : StringRequest (
            Method.POST, url,
            { response ->

                //Chiamata andata a buon fine
                LoginHelper.utenteConnesso = NetworkParser.parseUser(response)
                LoginHelper.save(this)

                //Utente loggato apro la homepage
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()

            },
            {error ->

                val description = NetworkParser.parseErrorDescription(error)
                AlertHelper.showToast(this, description)

            }

                ){
            //Faccio l'override della funzione standard, per utilizzare i miei parametri
            override fun getParams(): MutableMap<String, String>
            {
                return parameters
            }

        }

        //Eseguo la richiesta
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}