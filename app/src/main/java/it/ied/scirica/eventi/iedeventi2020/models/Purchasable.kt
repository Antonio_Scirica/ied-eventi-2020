package it.ied.scirica.eventi.iedeventi2020.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Purchasable
    (
        open var id: Int? = null,
        open var name:String? = null,
        open var description: String? = null,
        open var imageUrl: String? = null,
        open var quantity: Int = 0,
        open var price: Double = 0.0
): Parcelable
{

}