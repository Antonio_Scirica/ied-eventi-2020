package it.ied.scirica.eventi.iedeventi2020.helpers

import android.content.Context
import android.icu.text.CaseMap
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.security.AccessControlContext
import java.time.Duration

typealias EmptyCallback = () -> Unit
typealias BooleanCallBack  =  (Boolean) -> Unit

class AlertHelper
{
    companion object
    {
        fun showSimpleAlert(context: Context, title: String?, message: String? = null, callback: BooleanCallBack? = null)
        {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setCancelable(false)

            builder.setPositiveButton("OK")
            {
                    dialog, which ->
                callback?.invoke(true)

            }


            builder.show()
        }

        fun showConfirmationAlert(context: Context, title: String?, message: String? = null, callback: BooleanCallBack? = null)
        {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setCancelable(false)

            builder.setPositiveButton("Si")
            {
                dialog, which ->
                callback?.invoke(true)

            }
            builder.setNegativeButton("No")
            {
                    dialog, which ->
                callback?.invoke(false)

            }

            builder.show()
        }

        fun showToast(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT)
        {
            Toast.makeText(context, message, duration).show()
        }
    }
}