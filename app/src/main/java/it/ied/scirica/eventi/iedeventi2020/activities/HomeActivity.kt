package it.ied.scirica.eventi.iedeventi2020.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.fragments.*

class HomeActivity : BaseEventsActivity() {

    private var mCurrentFragment: BaseFragment? = null

    override fun getActivityLayout(): Int {
        return R.layout.activity_home

    }

    override fun setupActivityData() {
        super.setupActivityData()

        val fragment = EventsPagerFragment()
        fragment.events = this.events
        replaceCurrentFragment(fragment)
    }

    private fun replaceCurrentFragment(with: BaseFragment)
    {
        supportFragmentManager.beginTransaction().replace(R.id.main_container, with).commit()
        mCurrentFragment = with
    }

    override fun selectedTabChanged(index: Int) {
        when(index) {
            0 -> {
                if (mCurrentFragment is EventsMapFragment) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata Home
                    val intent = Intent(applicationContext, MapsActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            1 -> {
                if (mCurrentFragment is EventsPagerFragment) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata della Mappa
                    val intent = Intent(applicationContext, EventsPagerFragment::class.java)
                    startActivity(intent)
                    finish()
                }

            }
            2 -> {
                if (mCurrentFragment is ProfileFragment) {
                    //Non apro sempre la stessa pagina
                } else {
                    val fragment = ProfileFragment()
                    replaceCurrentFragment(fragment)
                }
            }
        }
    }
}
