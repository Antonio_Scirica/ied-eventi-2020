package it.ied.scirica.eventi.iedeventi2020.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.adapters.ShoppingCartAdapters
import it.ied.scirica.eventi.iedeventi2020.helpers.AlertHelper
import it.ied.scirica.eventi.iedeventi2020.helpers.ShoppingCartHelper
import kotlinx.android.synthetic.main.activity_shopping_cart.*
import kotlin.math.roundToInt

class ShoppingCartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)

        upadateActivityData()

        title = "Carrello"

        button_clear.text = "Svuota Carrello"

        button_clear.setOnClickListener {
            showClearCartDialog()
        }

        //Inizializzo la recycle view
        recycle_view.layoutManager = LinearLayoutManager(this)

        val adapter = ShoppingCartAdapters(ShoppingCartHelper.items, this)
        recycle_view.adapter = adapter

        //Aggiorno la schermata
        upadateActivityData()
    }

    fun upadateActivityData()
    {
        //Calcolo il costo totale
        var totalPrice = 0.0

        for(item in ShoppingCartHelper.items)
        {
            totalPrice += item.price
        }

        text_total.text = "Il totale del carrello è: ${totalPrice.roundToInt()} €"

        //Aggiorno la lista degli elementi
        val adapter = recycle_view.adapter

        if(adapter is ShoppingCartAdapters)
        {
            adapter.items = ShoppingCartHelper.items
            adapter.notifyDataSetChanged()
        }
    }
    
    fun showClearCartDialog()
    {
        AlertHelper.showConfirmationAlert(this,  "Conferma Operazione", "Sicuro di voler svuotare il carrello?" )
        { result ->
            if(result)
            {
                //Svuota la lista del carrello
                ShoppingCartHelper.clearCart()
                upadateActivityData()

                AlertHelper.showToast(this, "Il carrello ora è vuoto")
            }
        }
    }
}