package it.ied.scirica.eventi.iedeventi2020.models;

public class UploadBuildConfig {
        public static final boolean DEBUG = Boolean.parseBoolean("true");
        public static final String APPLICATION_ID = "it.ied.scirica.eventi";
        public static final String BUILD_TYPE = "debug";
        public static final int VERSION_CODE = 1;
        public static final String VERSION_NAME = "1.0";
}
