package it.ied.scirica.eventi.iedeventi2020.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.models.Event
import kotlinx.android.synthetic.main.layout_tabbar.*
import java.text.SimpleDateFormat

abstract class BaseEventsActivity : AppCompatActivity()
{
    var events: MutableList<Event> = mutableListOf()

    abstract fun getActivityLayout(): Int

    open fun setupActivityData()
    {
        //Questa funzione va inplementata nelle sottoclassi che vogliono inizializzare la schermata
        //Creo alcuni eventi di prova
        events.add(
            Event(
                name = "Evento Uno",
                description = getString(R.string.descrizione_avento),
                price = 100.0,
                address = "Dolomiti",
                imageUrl = "https://images.unsplash.com/photo-1598524638838-ea812a97b97a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("10/06/2020 10:30"),
                latitude = 42.0,
                longitude = 12.0
            )
        )
        events.add(
            Event(
                name = "Evento Due",
                price = 80.0,
                description = getString(R.string.descrizione_avento),
                address = "Da qualche parte",
                imageUrl = "https://images.unsplash.com/photo-1598524638838-ea812a97b97a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("20/06/2020 10:30"),
                latitude = 50.0,
                longitude = 2.0
            )
        )
        events.add(
            Event(
                name = "Evento Tre",
                description = getString(R.string.descrizione_avento),
                price = 50.0,
                address = "bhooo",
                imageUrl = "https://images.unsplash.com/photo-1598524638838-ea812a97b97a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("24/01/2020 10:30"),
                latitude = 54.0,
                longitude = 20.0

            )
        )
        events.add(
            Event(
                name = "Evento Quattro",
                description = getString(R.string.descrizione_avento),
                price = 20.0,
                address = "verso l'infinito e oltre",
                imageUrl = "https://images.unsplash.com/photo-1598524638838-ea812a97b97a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80",
                date = SimpleDateFormat("dd/MM/yyyy HH:mm").parse("12/08/2020 10:30"),
                latitude = 69.0,
                longitude = 30.0
            )
        )

        //Inizializzo la Tab bar
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                selectedTabChanged(tab?.position ?: 0)

            }
            override fun onTabReselected(tab: TabLayout.Tab?) {
                selectedTabChanged(tab?.position ?: 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?)
            {

            }
        })
    }


    open fun updateActivityData()
    {

    }

    internal open fun selectedTabChanged(index: Int) {
        //Funzione che viene richiamata quando viene selezionato un TabItem
        when(index) {
            0 -> {
                if (this is MapsActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata Home
                    val intent = Intent(applicationContext, MapsActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            1 -> {
                if (this is HomeActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata della Mappa
                    val intent = Intent(applicationContext, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            2 -> {
                if (this is ProfileActivity) {
                    //Non apro sempre la stessa pagina
                } else {
                    //E' stata selezionata la schermata del profilo
                    val intent = Intent(applicationContext, ProfileActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getActivityLayout())
        setupActivityData()
        updateActivityData()
    }
}