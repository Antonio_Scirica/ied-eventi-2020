package it.ied.scirica.eventi.iedeventi2020.activities

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import it.ied.scirica.eventi.iedeventi2020.*
import it.ied.scirica.eventi.iedeventi2020.adapters.EventAdapter
import it.ied.scirica.eventi.iedeventi2020.adapters.OnEventClickListener
import it.ied.scirica.eventi.iedeventi2020.helpers.NetworkParser
import it.ied.scirica.eventi.iedeventi2020.models.Event
import kotlinx.android.synthetic.main.activity_events.*

class EventsActivity : BaseEventsActivity(), OnEventClickListener {

    override fun getActivityLayout(): Int {
        return R.layout.activity_events
    }


    override fun setupActivityData() {
        super.setupActivityData()

        title = "Home"

        //Inizializzare la Recycle view
        recycler_view.layoutManager = LinearLayoutManager(this)

        val adapter = EventAdapter(events, this)
        adapter.onEventClickListener = this
        recycler_view.adapter = adapter

        updateEventsList()


    }
/*
        //Test prima chiamata internet
        val url = "http://ied.apptoyou.it/app/meteoroma.php"

        //Creo la richiesta web
        val request = StringRequest(Request.Method.GET, url,
            {response ->
                Toast.makeText(this, "Meteo di Roma: $response", Toast.LENGTH_SHORT).show()
            },
            {error ->
                Toast.makeText(this, "Errore:", Toast.LENGTH_SHORT).show()
            })
        //Richiesta al server
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

 */

    fun updateEventsList()
    {
        val url = "http://ied.apptoyou.it/app/api/eventi.php"

        val request = StringRequest(Request.Method.GET, url,
            {response ->

                //Il server ha inviato correttamente la lista degli eventi
                val eventsList = NetworkParser.parseEventList(response)


                //Compila solo se è un EventAdapter(Aggiorno la lista degli eventi rappresentati)
                (recycler_view.adapter as? EventAdapter)?.setEventsList(eventsList)


            },
            {error ->

                //Errore sul server
                Toast.makeText(this, "Errore:", Toast.LENGTH_SHORT).show()
            })

        //Richiesta al server
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }




    override fun onEventClick(event: Event)
    {
        //Apro la schermata dell'evento selezionato
        val intent = Intent(this, EventDetailActivity::class.java)

        //Gli passo l'evento selezionato
        intent.putExtra("event", event)

        //Vado alla nuova schermata
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menuitem_shoppingcart, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.button_shoppingcart)
        {
            //E' stato selezionato il pulsante del carrello
            val intent = Intent(this, ShoppingCartActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}


