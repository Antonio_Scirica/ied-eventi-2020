package it.ied.scirica.eventi.iedeventi2020.helpers

import com.android.volley.VolleyError
import it.ied.scirica.eventi.iedeventi2020.models.Event
import it.ied.scirica.eventi.iedeventi2020.models.User
import org.json.JSONObject
import java.text.SimpleDateFormat

class NetworkParser {

    companion object {
        fun parseErrorDescription(error: VolleyError): String? {
            val response = String(error.networkResponse.data)
            val responseObject = JSONObject(response)

            val errorObject = responseObject.optJSONObject("error")
            return errorObject?.optString("description")
        }

        fun parseUser(response: String): User? {
            val responseObject = JSONObject(response)
            val dataObject = responseObject.optJSONObject("data")

            return parseUser(dataObject)
        }

        fun parseUser(dataObject: JSONObject?) : User
        {
            val user = User()

            user.authToken = dataObject?.optString("auth_token")
            user.id = dataObject?.optInt("id_utente").toString()

            user.firsName = dataObject?.optString("nome")
            user.lastName = dataObject?.optString("cognome")
            user.city = dataObject?.optString("citta")
            user.avatarUrl = dataObject?.optString("avatar_url")
            user.birthDate = dataObject?.optString("data_nascita")
            user.email = dataObject?.optString("email")

            return user
        }

        fun parseEventList(response: String) : List<Event>{
            val responseObject = JSONObject(response)
            val dataArray = responseObject.optJSONArray("data")

            var eventsList = mutableListOf<Event>()
            val maxCounter = dataArray?.length() ?: 0

            for (counter in 0 until maxCounter) {

                //Prendo l'oggetto dell'array JSON

                val dataObject = dataArray?.optJSONObject(counter)

                //Trasforma l'oggetto JSON in un Event
                val event = parseEvent(dataObject)

                //Lo aggiungo alla lista degli eventi
                eventsList.add(event)
            }

            return eventsList
        }
        fun parseEvent(dataObject: JSONObject?) : Event {

            val event = Event()

            event.id = dataObject?.optInt("id")
            event.name = dataObject?.optString("nome")
            event.description = dataObject?.optString("descrizione")
            event.imageUrl = dataObject?.optString("cover_url")
            event.price = dataObject?.optDouble("prezzo") ?: 0.0

            event.address = dataObject?.optString("indirizzo")
            event.latitude = dataObject?.optDouble("lat")
            event.longitude = dataObject?.optDouble("lng")

            val stringDate = dataObject?.optString("timestamp") ?: ""
            event.date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(stringDate)

            event.viewsCount= dataObject?.optInt("numero_visualizzazioni") ?: 0
            event.commentsCount= dataObject?.optInt("numero_commenti") ?: 0
            event.likesCount= dataObject?.optInt("numero_like") ?: 0

            val creatorObject = dataObject?.optJSONObject("creatore")
            event.creator = parseUser(creatorObject)

            return event

        }
    }
}