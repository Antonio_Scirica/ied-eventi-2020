package it.ied.scirica.eventi.iedeventi2020.fragments

import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.tabs.TabLayoutMediator
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.adapters.EventAdapter
import it.ied.scirica.eventi.iedeventi2020.adapters.EventsPagerAdapter
import it.ied.scirica.eventi.iedeventi2020.helpers.AlertHelper
import it.ied.scirica.eventi.iedeventi2020.helpers.NetworkParser
import it.ied.scirica.eventi.iedeventi2020.models.Event
import kotlinx.android.synthetic.main.activity_events.*
import kotlinx.android.synthetic.main.fragment_events_pager.*

class EventsPagerFragment : BaseFragment()
{
    var events: List<Event> = ArrayList()


    override fun getFragmentLayout(): Int {
        return R.layout.fragment_events_pager
    }

    override fun setupFragmentData() {
        super.setupFragmentData()

        val adapter = activity?.let{
            EventsPagerAdapter(it) }
        adapter?.events = this.events
        view_pager.adapter = adapter

        TabLayoutMediator(tab_layout, view_pager) { tab, position ->
            if(position == 0)
            {
                tab.text = "LISTA"
            }
            else
            {
                tab.text = "MAPPA"
            }
        }.attach()

        updateEvents()
    }

    fun updateEvents()
    {
        val url = "http://ied.apptoyou.it/app/api/eventi.php"

        val request = StringRequest(
            Request.Method.GET, url,
            {response ->

                //Il server ha inviato correttamente la lista degli eventi
                val updatedEvents = NetworkParser.parseEventList(response)


                //Compila solo se è un EventAdapter(Aggiorno la lista degli eventi rappresentati)
                (view_pager.adapter as? EventsPagerAdapter)?.events = updatedEvents

                //AlertHelper.showToast(activity, "Eventi caricati: ${updatedEvents.size}")


            },
            {error ->

                //Errore sul server
                AlertHelper.showToast(activity, "Errore:")
            })

        //Richiesta al server
        val queue = Volley.newRequestQueue(activity)
        queue.add(request)
    }
}