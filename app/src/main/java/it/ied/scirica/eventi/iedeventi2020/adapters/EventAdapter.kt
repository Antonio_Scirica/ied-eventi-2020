package it.ied.scirica.eventi.iedeventi2020.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import it.ied.scirica.eventi.iedeventi2020.models.Event
import it.ied.scirica.eventi.iedeventi2020.R
import kotlinx.android.synthetic.main.listitem_event.view.*
import java.text.SimpleDateFormat
import java.util.*

typealias EventCallback = (Event) -> Unit

class EventAdapter
    (
    var items: List<Event>,
    val context: Context
) : RecyclerView.Adapter<ViewHolder>()
{

    var onEventClickListener: OnEventClickListener? = null
    var onEventClickCallback: EventCallback? = null


    fun setEventsList(eventsList: List<Event>)
    {
        items = eventsList
        notifyDataSetChanged()
    }



    override fun getItemCount(): Int {
        //Funzione che comunica quanti elementi bisogna rappresentare
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //Funzione che crea il layout
        val view = LayoutInflater.from(context).inflate(R.layout.listitem_event, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //Funziona che popola il layout con i dati degli eventi
        val event = items[position]

        val dataEvento = event.date ?: Date()
        holder.textNomeGiorno.text = SimpleDateFormat("E").format(dataEvento)
        holder.textNumeroGiorno.text = SimpleDateFormat("dd").format(dataEvento)

        holder.textName.text = event.name
        holder.textAddress.text = event.address


        if(event.imageUrl != null)
        {
            Picasso.get().load(event.imageUrl).fit().centerCrop().into(holder.imageBanner)
        }
        else
        {
            holder.imageBanner.setImageDrawable(null)
        }


        //Intercetto il click dell'elemento sulla lista
        holder.itemView.setOnClickListener {
            onEventClickListener?.onEventClick(event)
            onEventClickCallback?.invoke(event)
        }
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
{
    val textNomeGiorno= view.text_nome_giorno
    val textNumeroGiorno = view.text_numero_giorno

    val textName = view.text_name
    val textAddress = view.text_address

    val imageBanner = view.image_banner
}

interface OnEventClickListener {
    fun onEventClick(event: Event)
}