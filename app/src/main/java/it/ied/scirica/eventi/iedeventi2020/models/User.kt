package it.ied.scirica.eventi.iedeventi2020.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(
    var id: String? = null,
    var authToken: String? = null,
    var email: String? = null,
    var firsName: String? = null,
    var lastName: String? = null,
    var city: String? = null,
    var birthDate: String? = null,
    var avatarUrl: String? = null

) : Parcelable
{
    //Variabili calcolate a runtime
    val fullName: String
    get() {
        if (firsName != null && lastName != null)
        {
            //Nome + Cognome
            return "$firsName $lastName"
        }
        else if(firsName != null)
        {
            //Nome
            return "$firsName"
        }
        else if(lastName != null)
        {
            //Cognome
            return "$lastName"
        }
        else
        {
            //Nessuno dei due
            return ""
        }

    }
}