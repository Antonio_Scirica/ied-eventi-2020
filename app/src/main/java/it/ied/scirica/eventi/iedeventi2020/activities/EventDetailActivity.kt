package it.ied.scirica.eventi.iedeventi2020.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import it.ied.scirica.eventi.iedeventi2020.models.Event
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.helpers.ShoppingCartHelper
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.activity_event_detail.text_name
import org.json.JSONObject
import java.text.SimpleDateFormat
import kotlin.math.roundToInt

class EventDetailActivity : AppCompatActivity() {

    var event: Event? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        title = "Evento"

        //Prendo l'evento passato dalla schermata precedente
        event = intent.getParcelableExtra("event")

        //Popolo la schermata con le informazioni dell'evento
        text_name.text = event?.name
        text_address.text = event?.address
        text_description.text = event?.description
        text_likes.text = event?.likesCount.toString()
        text_views.text = event?.viewsCount.toString()
        text_commenti.text = event?.commentsCount.toString()

        if(event?.date != null)
        {
            text_date.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(event?.date)
        }
        else
        {
            text_date.text = "Nessuna data inserita"
        }

        //Prezzo biglietti
        if(event?.price != null && event?.price!! <= 0.0)
        {
            button_buy_ticket.text = "GRATIS"
        }
        else
        {
            button_buy_ticket.text = "Biglietto ${event?.price?.roundToInt()} €"
        }


        //Info creatore evento
        text_creator.text = event?.creator?.fullName

        if(event?.creator?.avatarUrl != null)
        {
            Picasso.get().load(event?.creator?.avatarUrl).fit().centerCrop().into(image_creator)
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            image_creator.clipToOutline = true
        }



        //Immagine Banner
        if(event?.imageUrl != null)
        {
            Picasso.get().load(event?.imageUrl).fit().centerCrop().into(image_banner)
        }
        //Interazione con il pulsante di acquisto
        button_buy_ticket.setOnClickListener{
            //Aggiungo l'evento al carrello

            //Prima controllo se l'evento esiste
            if(event != null)
            {
                ShoppingCartHelper.addItem(event!!)
                val message =("Numero di oggetti nel carrello: ${ShoppingCartHelper.items.count()}")
                //PopUp scritto non interagibile nell'app
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            }
        }

        text_creator.setOnClickListener {
            openEventCreator()
        }
        image_creator.setOnClickListener {
            openEventCreator()
        }

        updateWeatherData()
    }

    private fun openEventCreator()
    {

            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra("selectedUser", event?.creator)
            startActivity(intent)

    }

    fun updateWeatherData()
    {

        val lat = event?.latitude ?: 0.0
        val lon = event?.longitude ?: 0.0
        //Chiedo meteo del luogo dell'evento
        val url ="http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=89513d576b24e3c92c302c681473d6f9&units=metric&lang=it"

        //Creo la richiesta web
        val request = StringRequest(
            Request.Method.GET, url,
            {response ->
                val jsonObject = JSONObject(response)
                val mainObject = jsonObject.optJSONObject("main")
                val temp = mainObject?.optDouble("temp")

                val weatherArray = jsonObject.optJSONArray("weather")
                val weatherObject = weatherArray?.optJSONObject(0)
                val description = weatherObject?.optString("description")

                text_weather.text = "$temp °C - $description"

            },
            {error ->
                Toast.makeText(this, "Errore:", Toast.LENGTH_SHORT).show()
            })
        //Richiesta al server
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}