package it.ied.scirica.eventi.iedeventi2020.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.activities.EventDetailActivity
import it.ied.scirica.eventi.iedeventi2020.adapters.EventAdapter
import it.ied.scirica.eventi.iedeventi2020.adapters.OnEventClickListener
import it.ied.scirica.eventi.iedeventi2020.models.Event
import kotlinx.android.synthetic.main.activity_events.*

class EventsListFragment : BaseFragment(){

    var events: List<Event> = ArrayList()
    set(value) {
        field = value
        (recycler_view?.adapter as? EventAdapter)?.setEventsList(value)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_events_list
    }

    override fun setupFragmentData() {
        super.setupFragmentData()

        //Inizializzare la Recycle view
        recycler_view.layoutManager = LinearLayoutManager(activity)

        val adapter = activity?.let { EventAdapter(events, it) }
        adapter?.onEventClickCallback = {

            //Apro la schermata dell'evento selezionato
            val intent = Intent(activity, EventDetailActivity::class.java)

            //Gli passo l'evento selezionato
            intent.putExtra("event", it)

            //Vado alla nuova schermata
            startActivity(intent)
        }
        recycler_view.adapter = adapter
    }

}