package it.ied.scirica.eventi.iedeventi2020.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.models.Purchasable
import kotlinx.android.synthetic.main.listitem_shoppingcart.view.*
import kotlin.math.roundToInt

class ShoppingCartAdapters
    (

    var items: MutableList<Purchasable>,
    var context: Context
) : RecyclerView.Adapter<ShoppingCartViewHolder>()
{
    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingCartViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.listitem_shoppingcart, parent, false)
        return ShoppingCartViewHolder(view)
    }

    override fun onBindViewHolder(holder: ShoppingCartViewHolder, position: Int) {
        //Funzione che popola il layout con i dati dei singoli elementi da rappresentare
        val purchasable = items[position]

        holder.textNome.text = purchasable.name
        holder.textPrice.text = "${purchasable.price.roundToInt()} €"
    }
}

class ShoppingCartViewHolder(view: View) : RecyclerView.ViewHolder(view)
{
    val textNome = view.text_name
    val textPrice = view.text_price
}