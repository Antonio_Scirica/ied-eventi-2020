package it.ied.scirica.eventi.iedeventi2020.helpers

import it.ied.scirica.eventi.iedeventi2020.models.Purchasable


class ShoppingCartHelper
{
    companion object
    {
        var items: MutableList<Purchasable> = mutableListOf()


        fun addItem(item: Purchasable) {
            //Controllo che l'oggetto non sia già stato inserito
            if (items.contains(item))
            {
                //Non devo aggiungerlo un'altra volta
            }
            else
            {
                items.add(item)
            }
        }
        fun clearCart()
        {
            items = mutableListOf()
        }
    }
}