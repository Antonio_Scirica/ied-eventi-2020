package it.ied.scirica.eventi.iedeventi2020.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import it.ied.scirica.eventi.iedeventi2020.R


class ProfileFragment : BaseFragment() {
    override fun getFragmentLayout(): Int
    {
        return R.layout.fragment_profile
    }
}