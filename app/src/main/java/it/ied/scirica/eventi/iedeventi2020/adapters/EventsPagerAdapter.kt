package it.ied.scirica.eventi.iedeventi2020.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import it.ied.scirica.eventi.iedeventi2020.fragments.EventsListFragment
import it.ied.scirica.eventi.iedeventi2020.fragments.EventsMapFragment
import it.ied.scirica.eventi.iedeventi2020.models.Event

class EventsPagerAdapter(var activity: FragmentActivity) : FragmentStateAdapter(activity)
{
    var events: List<Event> = ArrayList()
    set(value)
    {
        field = value
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return 2
    }


    override fun createFragment(position: Int): Fragment {
        if(position == 0)
        {
            val fragment = EventsListFragment()
            fragment.events = this.events
            return fragment
        }
        else
        {
            val fragment = EventsMapFragment()
            fragment.events = this.events

            return fragment
        }
    }

    override fun onBindViewHolder(holder: FragmentViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)

        val tag = "f" + holder.itemId
        val fragment = activity.supportFragmentManager.findFragmentByTag(tag)

        when(fragment)
        {
            is EventsListFragment ->
                fragment.events = this.events

            is EventsMapFragment ->
                fragment.events = this.events

        }
    }

}