package it.ied.scirica.eventi.iedeventi2020.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import it.ied.scirica.eventi.iedeventi2020.R
import it.ied.scirica.eventi.iedeventi2020.helpers.LoginHelper
import it.ied.scirica.eventi.iedeventi2020.helpers.NetworkParser
import it.ied.scirica.eventi.iedeventi2020.models.User
import kotlinx.android.synthetic.main.activity_profile.*
import net.gotev.uploadservice.data.UploadInfo
import net.gotev.uploadservice.network.ServerResponse
import net.gotev.uploadservice.observer.request.RequestObserverDelegate
import net.gotev.uploadservice.protocols.multipart.MultipartUploadRequest
import java.io.File
import java.io.FileOutputStream


class ProfileActivity : BaseEventsActivity() {

    var selectedUser: User? = null

    private val mIsLoggedUser: Boolean
        get()
        {
            return selectedUser == null
        }

    override fun getActivityLayout(): Int {
        return R.layout.activity_profile
    }


    override fun setupActivityData() {
        super.setupActivityData()

        title = "Profilo"

        selectedUser = intent.getParcelableExtra("selectedUser")

        text_cambio_immagine.setOnClickListener{
            if(mIsLoggedUser)
            {
                openPhotoGallery()
            }

        }

        image_back_arrow.setOnClickListener {

            val intent = Intent(this, EventsActivity::class.java)
            startActivity(intent)
            finish()

        }

        button_logout.setOnClickListener {
            //Effettuo il LogOut e torno alla pagina di Login
            LoginHelper.utenteConnesso = null
            LoginHelper.save(this)

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        updateProfile()
    }

    override fun updateActivityData() {
        super.updateActivityData()

        val user = if(selectedUser != null) selectedUser else LoginHelper.utenteConnesso

        if(user?.avatarUrl != null)
        {
            Picasso.get().load(user.avatarUrl).into(image_avatar)
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            image_avatar.clipToOutline = true
        }

        text_name.text = user?.fullName
        text_city.text = user?.city
        text_email.text = user?.email
        text_birthDate.text = user?.birthDate

        if(!mIsLoggedUser)
        {
            button_logout.visibility = View.GONE
        }
        else
        {
            button_logout.visibility = View.VISIBLE
        }
    }

    private fun updateProfile()
    {
        val url = "http://ied.apptoyou.it/app/utente.php"

        val headers = HashMap<String, String>()
        headers["Authorization"] = "Token ${LoginHelper.utenteConnesso?.authToken}"

        val request = object: StringRequest(
            Request.Method.GET, url,
            {response ->

                //Prendo l'utente aggiornato
                val updateUser = NetworkParser.parseUser(response)

                LoginHelper.utenteConnesso = updateUser
                LoginHelper.save(this)

                //Ricarico la pagina
                updateActivityData()
            },
            {error ->

                //Errore sul server
                Toast.makeText(this, "Errore:", Toast.LENGTH_SHORT).show()
            }) {
            //Faccio l'verride della funzione standard per utilizzare i miei header
            override fun getHeaders(): MutableMap<String, String> {
                return headers
            }
        }

        //Richiesta al server
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    private fun openPhotoGallery()
    {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, 1001)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 1001 && resultCode == Activity.RESULT_OK)
        {
            val imageUri = data?.data

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val source =ImageDecoder.createSource(contentResolver, imageUri!!)
                val bitmap = ImageDecoder.decodeBitmap(source)
                uploadAvatar(bitmap)
            }
            else
            {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                uploadAvatar(bitmap)
            }


        }
    }

    private fun uploadAvatar(source: Bitmap)
    {
        //Prendere immagine drawable
        //var bitmap = BitmapFactory.decodeResource(resources, R.drawable.immagine_profilo_nuovo)

        //Ridimensionare immagine
        var bitmap = resizeBitmap(source, 600)

        //Immagine su file temporaneo
        val storagePath = externalCacheDir.toString()
        val destinationPath = "$storagePath/avatar.jpg"

        val file = File(destinationPath)
        val outputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream)
        outputStream.flush()
        outputStream.close()

        //Inviarla al server in Multipart POST
        val url = "http://ied.apptoyou.it/app/modifica_avatar.php"


       /* MultipartUploadRequest(this, url).setMethod("POST")
            .addHeader("Authorization", "Token ${LoginHelper.utenteConnesso?.authToken}")
            .addFileToUpload(destinationPath, "avatar")
            .startUpload()

        */
        MultipartUploadRequest(this, url).setMethod("POST")
            .addHeader("Authorization", "Token ${LoginHelper.utenteConnesso?.authToken}")
            .addFileToUpload(destinationPath, "avatar")
            .subscribe(this, this, object : RequestObserverDelegate
            {
                override fun onCompleted(context: Context, uploadInfo: UploadInfo) {

                }

                override fun onCompletedWhileNotObserving() {

                }

                override fun onError(
                    context: Context,
                    uploadInfo: UploadInfo,
                    exception: Throwable
                ) {

                }

                override fun onProgress(context: Context, uploadInfo: UploadInfo) {

                }

                override fun onSuccess(
                    context: Context,
                    uploadInfo: UploadInfo,
                    serverResponse: ServerResponse
                ) {
                        updateProfile()
                }

            })



    }

    private fun resizeBitmap(source: Bitmap, maxLength: Int): Bitmap {
        try {
            if (source.height >= source.width) {
                if (source.height <= maxLength) { // if image height already smaller than the required height
                    return source
                }

                val aspectRatio = source.width.toDouble() / source.height.toDouble()
                val targetWidth = (maxLength * aspectRatio).toInt()
                return Bitmap.createScaledBitmap(source, targetWidth, maxLength, false)
            } else {
                if (source.width <= maxLength) { // if image width already smaller than the required width
                    return source
                }

                val aspectRatio = source.height.toDouble() / source.width.toDouble()
                val targetHeight = (maxLength * aspectRatio).toInt()

                return Bitmap.createScaledBitmap(source, maxLength, targetHeight, false)
            }
        } catch (e: Exception) {
            return source
        }
    }


}